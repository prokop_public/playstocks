package controllers;

import java.util.*;

import play.mvc.*;
import play.db.jpa.*;

import models.*;

import views.html.*;

import javax.persistence.*;

import com.google.gson.*;

public class Application extends Controller {

    @Transactional(readOnly=true)
    public static Result index() {
		User user = User.authenticate("", "");
        List<Company> allCompanies = Company.getAllCompanies();
        Date currentDate = new Date();
        Calendar currentCal = Calendar.getInstance();
        for(Company c: allCompanies) {
            List<StockPriceHistory> history = StockPriceHistory.getAllForCompanyId(c.id);
            //System.out.println("C: " + c.name + " hist: " + history);
            if(0 == history.size()) {
                currentCal.setTime(currentDate);
                currentCal.add(Calendar.SECOND, 5 * 500);
                JPA.em().getTransaction().begin();
                for (int i = 0; i < 500; i++) {
                    StockPriceHistory h = new StockPriceHistory();
                    h.company_id = c.id;
                    currentCal.add(Calendar.SECOND, 5);
                    h.price_date = currentCal.getTime();
                    h.price = new Float(Math.round(Math.random() * (40 + i)) + 100 + i);
                    JPA.em().persist(h);
                }
                JPA.em().getTransaction().commit();
            }
        }

        for(Company c: allCompanies) {
            List<StockPriceHistory> history = StockPriceHistory.getAllForCompanyId(c.id);
            System.out.println("C: " + c.name + " hist:");
            for(StockPriceHistory h: history) {
                System.out.println(h.price_date + ": " + h.price);
            }
        }

        return ok(index.render("Super awesome stock exchange", allCompanies, user));
    }

    @Transactional(readOnly=true)
    public static Result getStockQuotes() {
        List<StockPriceHistory> history = StockPriceHistory.getAllForCompanyId(1L);
        String json = new Gson().toJson(history);
        return ok(json);
    }
}
