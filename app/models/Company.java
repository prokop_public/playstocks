package models;

import java.util.*;

import javax.persistence.*;

import play.data.validation.*;
import play.db.jpa.*;

@Entity
@Table(name = "companies")
@NamedQuery(name = "Company.getAll",
            query = "SELECT c FROM Company c ")
public class Company {

    @Id
    public Long id;

    @Constraints.Required
    public String name;

    @Constraints.Required
    public String ticker;

    public static Company findById(Long id) {
        return JPA.em().find(Company.class, id);
    }

    public static List<Company> getAllCompanies() {
        List<Company> companies = JPA.em().createNamedQuery("Company.getAll", Company.class).getResultList();
        return companies;
    }

    public static Map<String,String> options() {
        List<Company> companies = JPA.em().createQuery("from companies order by name").getResultList();
        LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        for(Company c: companies) {
            options.put(c.id.toString(), c.name);
        }
        return options;
    }
}