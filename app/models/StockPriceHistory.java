package models;

import java.util.*;

import play.data.validation.Constraints;
import play.db.jpa.JPA;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;

@Entity
@Table(name = "stock_price_history")
@NamedQuery(name = "StockPriceHistory.getAllForCompanyId",
            query = "SELECT s FROM StockPriceHistory s WHERE s.company_id = :companyId")
public class StockPriceHistory {
    @Id
    @GeneratedValue
    public Long id;

    @Constraints.Required
    public Long company_id;

    @Constraints.Required
    public Date price_date;

    @Constraints.Required
    public Float price;

    public static StockPriceHistory findById(Long id) {
        return JPA.em().find(StockPriceHistory.class, id);
    }

    public static List<StockPriceHistory> getAllForCompanyId(Long id) {
        return JPA.em().createNamedQuery("StockPriceHistory.getAllForCompanyId", StockPriceHistory.class).setParameter("companyId", id).getResultList();
    }
}