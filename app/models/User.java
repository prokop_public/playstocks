package models;

import java.util.*;

import javax.persistence.*;

import play.data.validation.*;
import play.db.jpa.*;

@Entity
@Table(name = "users")
@NamedQuery(name = "User.authenticate",
            query = "SELECT u FROM User u WHERE u.email = 'prokop.petr@gmail.com' ")
// AND password = '1d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'
public class User {

    @Id
    public Long id;

    @Constraints.Required
    public String email;

    @Constraints.Required
    public String password;

    public static User findById(Long id) {
        return JPA.em().find(User.class, id);
    }

	public static User authenticate(String email, String password) {
        User result;
        result = JPA.em().createNamedQuery("User.authenticate", User.class).getSingleResult();

        return result;
    }
}