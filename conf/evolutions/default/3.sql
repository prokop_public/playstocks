# --- Users

# --- !Ups

create table users (
  id                        bigint not null,
  email                     varchar(255),
  password                  varchar(64),
  constraint pk_user primary key (id))
;

insert into users (id, email, password) values ( 1, 'prokop.petr@gmail.com', HASH('SHA256', STRINGTOUTF8('123456'), 1));

# --- !Downs

drop table if exists companies;