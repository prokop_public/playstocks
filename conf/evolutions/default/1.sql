# --- Database schema

# --- !Ups

create table companies (
  id                        bigint not null,
  name                      varchar(255),
  ticker                    varchar(16),
  constraint pk_company primary key (id))
;

# --- !Downs

drop table if exists companies;