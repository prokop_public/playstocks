# --- Stock price history

# --- !Ups

create table stock_price_history (
  id                        bigint not null AUTO_INCREMENT,
  company_id                bigint not null,
  price_date                datetime not null,
  price                     float not null,
  constraint pk_stock_price_history primary key (id))
;

# --- !Downs

drop table if exists stock_price_history;