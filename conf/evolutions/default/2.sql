# --- Dataset

# --- !Ups

insert into companies (id, name, ticker) values ( 1, 'Apple Inc.', 'APPL');
insert into companies (id, name, ticker) values ( 2, 'International Business Machines Corp.', 'IBM');
insert into companies (id, name, ticker) values ( 3, 'Facebook', 'FB');
insert into companies (id, name, ticker) values ( 4, 'Twitter Inc', 'TWTR');

# --- !Downs

delete from companies;